# martinreijns_nextflow

1. Log into Eddie

## Go to wildwest node
~~~
ssh node2c16
~~~

## Start screen session
~~~
screen -S mysessionname
~~~

## load java module 
~~~
module load roslin/java/11.0.12
~~~
## Download Nextflow
~~~
wget -qO- https://get.nextflow.io | bash
~~~

## Setup bash variables
~~~
export NXF_SINGULARITY_CACHEDIR=/exports/igmm/eddie/BioinformaticsResources/nfcore/singularity-images
export NXF_HOME=`pwd`/.nextflow
~~~

## Edit `nf-params.json`
Edit this file [nf-params.json](nf-params.json)

### igenomes dir
~~~
ls /exports/igmm/eddie/BioinformaticsResources/igenomes/
~~~

You will need to Set


## Create a samplesheet

Example [here](https://git.ecdf.ed.ac.uk/ggrimes2/martinreijns_nextflow/-/blob/main/RNAseqcsv_polyA.csv)

**Note** The path to the fastq files must be absolute e.g. `/export/igmm/eddie/myfolder/data/sample_1.R1.fastq.gz`

# example command

~~~
./nextflow run nf-core/rnaseq -r 3.8.1 -params-file nf-params.json -profile eddie -resume
~~~
