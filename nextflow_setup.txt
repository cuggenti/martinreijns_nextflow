#load java
module load roslin/java/11.0.12
wget -qO- https://get.nextflow.io | bash
export NXF_SINGULARITY_CACHEDIR=/exports/igmm/eddie/BioinformaticsResources/nfcore/singularity-images
export NXF_HOME=`pwd`/.nextflow

#example command
#./nextflow run nf-core/rnaseq -r 3.8.1 -params-file nf-params.json -profile eddie -resume
